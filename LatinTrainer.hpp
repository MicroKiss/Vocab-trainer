﻿#pragma once


#include <vector>
#include <string>
#include <iostream>
#include <cstdlib>
#include <algorithm> //transform
#include <fstream>
#include <windows.h>
#include <direct.h> //_mkdir



std::string LowerCase(std::string &str) {
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	return str;
}

struct WordPair
{
		std::string hun;
		std::string lat;

		WordPair(std::string hun, std::string lat) : hun(hun), lat(lat) {};
};




class Menu
{
public:
	Menu(bool &exit);
	~Menu() {};
	void ShowOptions();
private:
	void AddWord();
	void RemoveWord();
	void Practice() const;
	void ListWords() const;
private:
	bool *_exit;
	std::vector<WordPair> _vec;
	std::ifstream _infile;
	std::ofstream _outfile;

	std::string FilePath = "\\microkiss/vocab.txt" ;

};

Menu::Menu(bool &exit)
{
	_exit = &exit;
	
	char *pValue;   /// this 3 lines get the absolute path of the %appdata%
	size_t len;
	errno_t err = _dupenv_s(&pValue, &len, "APPDATA");
	FilePath = pValue + FilePath;
	_infile.open(FilePath, std::fstream::in);
	if (_infile.fail()) {
		std::cout << "Nem találtam adatbázist\n";
		std::string asd = "\\microkiss";
		_mkdir((pValue + asd).c_str());
		_outfile.open(FilePath);
		_outfile.close();
		return;
	}
	
	std::string hun, lat;

	_infile >> hun;
	_infile >> lat;
	while (!_infile.fail())
	{
		auto it = _vec.begin();
		while (it != _vec.end() && it->hun != hun) {
			++it;
		}
		if (it == _vec.end())
			_vec.push_back(WordPair(hun, lat));


		_infile >> hun;
		_infile >> lat;
	}
	_infile.close();
	return;
}//c-tor


void Menu::ShowOptions()
{
	std::cout
		<< "\n\n\n"
		<< "Válaszd ki a megfelelö menüpontot\n"
		<< "1. szavak gyakorlása\n"
		<< "2. Új szó hozzáadása\n"
		<< "3. Meglévö szó törlése\n"
		<< "4. Meglévö szavak listázása\n"
		<< "0. Kilépés\n";
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 1);
std::cout<< "°°-°°-°°-°°-°°-°°-°°-°°-°°-°°-°°-°°-°°-°°-°°-°°\n";
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);

	int bemenet;
	std::cin >> bemenet;

	while (std::cin.fail() || bemenet > 5){
		std::cout << bemenet;
		std::cout << "Nincs ilyen menüpont, próbáld újra\n";
		std::cin.clear();
		std::cin.ignore(128, '\n');
		std::cin >> bemenet;
	}
	
	switch (bemenet)
	{
	case 0:
		*_exit =true; return;	break;
	case 1:
		Practice(); break;
	case 2:
		AddWord(); break;
	case 3:
		RemoveWord(); break;
	case 4:
		ListWords(); break;

		
	default:
		std::cout << "Nem létezö menüpontot választottál, próbáld újra\n";
		break;
	}

	return;
}//ShowOptions

void Menu::AddWord()
{
	std::string word1, word2;
	std::string hungarianWord, latin;
	word1 = "a"; word2 = "b";
	while (LowerCase(word1) != LowerCase(word2))
	{
		std::cout << "Írd be a szót magyarul és nyomj entert \n";
		std::cin >> word1;
		std::cout << "Írd be mégegyszer \n";
		std::cin >> word2;
		LowerCase(word2);
		LowerCase(word1);
		if (word1 != word2)
			std::cout << "Nem egyezik a két szó, próbáld újra \n\n\n";
	}
	hungarianWord = word1;
	word1 = "a"; word2 = "b";

	while (LowerCase(word1) != LowerCase(word2))
	{
		std::cout << "Írd be a szót Latinul és nyomj entert \n";
		std::cin >> word1;
		std::cout << "Írd be mégegyszer \n";
		std::cin >> word2;
		LowerCase(word2);
		LowerCase(word1);
		if (word1 != word2)
			std::cout << "Nem egyezik a két szó, próbáld újra \n\n\n";
	}

	auto it = _vec.begin();
	while (it != _vec.end() && hungarianWord != it->hun)
		++it;

	if (it == _vec.end()) 
	{
		_outfile.open(FilePath, std::ios_base::app);
		_outfile << std::endl << hungarianWord << " " << word1 ;
		_outfile.flush();
		_outfile.close();
		_vec.push_back(WordPair(hungarianWord, word1)) ;


	}
	else std::cout << "Ez a szó már megvan\n";
	
	return;
}//AddWord

void Menu::RemoveWord() {
	std::cout << "Írd be azt a szót magyarul amit törölni szeretnél\n";
	std::string word;
	std::cin >> word;
	auto it = _vec.begin();
	while (it != _vec.end() && word != it->hun)
		++it;
	if (it != _vec.end())
	{
		*it = _vec.back();
		_vec.pop_back();
	}
	std::cout << word << " szó törölve az adatbázisból\n";
	std::ofstream file;
	file.open(FilePath);

	for (auto i : _vec)
	{
		file << i.hun << " " << i.lat << std::endl;
	}
	file.close();

	return;
}

void Menu::ListWords() const
{
	for (auto  it = _vec.begin(); it != _vec.end(); ++it)
	{
		std::cout << it->hun << " " << it->lat << std::endl;
	}

}

void Menu::Practice() const 
{
	if (!_vec.size())
	{
		std::cout << "Nincsenek szavak az adatbázisban\n";	return;
	}
	std::cout << "Hány szót szeretnél kapni? \n";
	int numberofwords;
	std::cin >> numberofwords;
	int counter = numberofwords;
	int good = 0;
	while (counter > 0)
	{
		int randomnumber = rand() % _vec.size();
		std::cout << _vec[randomnumber].hun << "  :";
		std::string input;
		std::cin >> input;
		if (LowerCase(input) == _vec[randomnumber].lat) 
		{
			std::cout << "Helyes!\n"; good++;
		}
		else 
		{
			std::cout << "Sajnos nem jó :( \n";
		}
		--counter;
	}

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (good > numberofwords/2 ? 10 :4) );

	std::cout <<"\n"<< numberofwords << "-ból eltaláltál :" << good << "-et\n";
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
	return;
}
